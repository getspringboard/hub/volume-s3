FROM mesosphere/aws-cli:1.14.5

ADD ./rootfs/ /

ENTRYPOINT ["/entrypoint"]

CMD ["/data"]
